package me.juan.learning;

import me.juan.learning.models.AirplaneSchedule;
import me.juan.learning.services.AirplaneService;
import me.juan.learning.services.HotelService;

//facade
public class TravelFinder {

    private final HotelService hotelService = new HotelService();
    private final AirplaneService airplaneService = new AirplaneService();

    public TravelResponse findHotelByOriginAndDestination(String origin, String destination) {
        String hotelName = hotelService.findHotelByCity(destination);
        AirplaneSchedule airplaneSchedule = airplaneService.findScheduleByOriginAndDestination(origin, destination);
        return new TravelResponse(airplaneSchedule, hotelName);
    }

}
