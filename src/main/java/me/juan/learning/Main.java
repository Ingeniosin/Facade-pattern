package me.juan.learning;

public class Main {

    public static void main(String[] args) {

        System.out.println(" ");
        TravelFinder travelFinder = new TravelFinder(); //facade
        TravelResponse travelResponse = travelFinder.findHotelByOriginAndDestination("Bogota", "Medellin");

        System.out.println(" ");
        if(travelResponse.getAirplaneSchedule() != null)
            System.out.println(" *  Encontramos un vuelo de [" + travelResponse.getAirplaneSchedule().getOrigin() + " -> " + travelResponse.getAirplaneSchedule().getDestination() + "] con una duración de: " + travelResponse.getAirplaneSchedule().getDuration() + " minutos");
        if(travelResponse.getHotelName() != null)
            System.out.println(" *  Encontramos un hotel en [" + travelResponse.getAirplaneSchedule().getDestination() + "] llamado: " + travelResponse.getHotelName());

    }

}
