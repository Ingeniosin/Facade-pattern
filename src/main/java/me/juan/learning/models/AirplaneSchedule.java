package me.juan.learning.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
public class AirplaneSchedule {
    private final String Uid = UUID.randomUUID().toString().substring(0, 4).toUpperCase();
    private String origin;
    private String destination;

    private Date departureDate;
    private Date arrivalDate;
    private int duration;

    public AirplaneSchedule(String origin, String destination, Date departureDate, int duration) {
        this.origin = origin;
        this.destination = destination;
        this.departureDate = departureDate;
        this.duration = duration;
        this.arrivalDate = new Date(departureDate.getTime() + (long) duration  * 60 * 1000);
    }
}
