package me.juan.learning;

import lombok.AllArgsConstructor;
import lombok.Data;
import me.juan.learning.models.AirplaneSchedule;

@Data
@AllArgsConstructor
public class TravelResponse {

    private AirplaneSchedule airplaneSchedule;
    private String hotelName;

}
