package me.juan.learning.services;

import me.juan.learning.models.AirplaneSchedule;

import java.util.*;

public class AirplaneService {

    private final List<AirplaneSchedule> airplaneSchedules = new ArrayList<>();

    public AirplaneService() {
        List<String> hotels = new ArrayList<>(HotelService.getHotels().keySet());
        Random random = new Random();
        hotels.forEach(h1 -> {
            hotels.forEach(h2 -> {
                if (!h1.equals(h2)) {
                    airplaneSchedules.add(new AirplaneSchedule(h1, h2, new Date(), random.nextInt(150) + 30));
                }
            });
        });
    }

    public AirplaneSchedule findScheduleByOriginAndDestination(String origin, String destination) {
        System.out.println("Finding schedule from " + origin + " to " + destination);
        Optional<AirplaneSchedule> schedule = airplaneSchedules.stream().filter(s -> s.getOrigin().equalsIgnoreCase(origin) && s.getDestination().equalsIgnoreCase(destination)).findFirst();
        return schedule.orElse(null);
    }

}
