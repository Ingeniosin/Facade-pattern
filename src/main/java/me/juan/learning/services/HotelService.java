package me.juan.learning.services;

import lombok.Getter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class HotelService {

    @Getter
    private static final Map<String, List<String>> hotels = new HashMap<>(){{
            put("cali", List.of("Hilton 1", "Hilton 2"));
            put("manizales", List.of("Marriot 1", "Marriot 2"));
            put("bogota", List.of("Sheraton 1", "Sheraton 2"));
            put("medellin", List.of("Hilton 1", "Hilton 2"));
            put("cartagena", List.of("Marriot 1", "Marriot 2"));
            put("barranquilla", List.of("Sheraton 1", "Sheraton 2"));
            put("cucuta", List.of("Hilton 1", "Hilton 2"));
            put("bucaramanga", List.of("Marriot 1", "Marriot 2"));
            put("pereira", List.of("Sheraton 1", "Sheraton 2"));
            put("armenia", List.of("Hilton 1", "Hilton 2"));
            put("bello", List.of("Marriot 1", "Marriot 2"));
            put("buenaventura", List.of("Sheraton 1", "Sheraton 2"));
            put("pasto", List.of("Hilton 1", "Hilton 2"));
        }
    };

    public String findHotelByCity(String city) {
        System.out.println("Finding hotels in " + city);
        Optional<String> hotel = hotels.getOrDefault(city.toLowerCase().trim(), List.of()).stream().findFirst();
        return hotel.orElse(null);
    }

}
